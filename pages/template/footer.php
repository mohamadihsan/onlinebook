        <!-- Jquery Core Js -->
        <script src="../assets/plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="../assets/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Select Plugin Js -->
        <script src="../assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="../assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
        
        <!-- Jquery Validation Plugin Css -->
        <script src="../assets/plugins/jquery-validation/jquery.validate.js"></script>

        <!-- JQuery Steps Plugin Js -->
        <script src="../assets/plugins/jquery-steps/jquery.steps.js"></script>

        <!-- Sweet Alert Plugin Js -->
        <script src="../assets/plugins/sweetalert/sweetalert.min.js"></script>

        <!-- Bootstrap Notify Plugin Js -->
        <script src="../assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    
        <!-- Waves Effect Plugin Js -->
        <script src="../assets/plugins/node-waves/waves.js"></script>

        <!-- Autosize Plugin Js -->
        <script src="../assets/plugins/autosize/autosize.js"></script>

        <!-- Moment Plugin Js -->
        <script src="../assets/plugins/momentjs/moment.js"></script>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="../assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Jquery DataTable Plugin Js -->
        <script src="../assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
        <script src="../assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
        <script src="../assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
        <script src="../assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

        <!-- Custom Js -->
        <script src="../assets/js/admin.js"></script>
        <script src="../assets/js/pages/ui/modals.js"></script>
        <script src="../assets/js/pages/forms/form-validation.js"></script>
        <script src="../assets/js/pages/forms/basic-form-elements.js"></script>
        <script src="../assets/js/pages/tables/jquery-datatable.js"></script>

        <!-- Demo Js -->
        <script src="../assets/js/demo.js"></script>

        <script>
            function msg_berhasil(pesan) {
                swal({
                    title: 'Berhasil!',
                    text: pesan,
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2000
                })    
            }

            function msg_gagal(pesan) {
                swal({
                    title: 'Gagal!',
                    text: pesan,
                    type: 'error',
                    showConfirmButton: false,
                    timer: 2000
                })    
            }

            function msg_info(pesan) {
                swal({
                    title: 'Informasi!',
                    text: pesan,
                    type: 'info',
                    showConfirmButton: true,
                    confirmButtonText: 'OK'
                })    
            }

            function formatRupiah(angka, prefix){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split           = number_string.split(','),
                sisa            = split[0].length % 3,
                rupiah          = split[0].substr(0, sisa),
                ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
     
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
     
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }
        </script>
    </body>
</html>