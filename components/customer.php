<?php

// Init Nama Menu 
$menu = 'customer';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();
?>
<title>Customer</title>

<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>
                BUKU ONLINE
                <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
            </h2>
        </div> -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            CUSTOMER
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <!-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul> -->
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Lengkap</th>
                                        <th class="text-center">Nomor Telepon</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Terdaftar tgl</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT fullname_var, 
                                                phone_number_var, 
                                                email_var, 
                                                address_var, 
                                                t_mtr_detail_user.created_on_dtm, 
                                                t_mtr_detail_user.updated_on_dtm  
                                            FROM t_mtr_user
                                            LEFT JOIN t_mtr_detail_user ON id_user_seq = id_user_int
                                            WHERE id_grup_user_int = 2";
                                    $result = mysqli_query($conn, $sql);
                                
                                    if (mysqli_num_rows($result) > 0) {
                                        $no = 1;
                                        while($row = mysqli_fetch_assoc($result)){
                                            if ($row['created_on_dtm'] != null) {
                                                $created_on_dtm = date('d-m-Y H:i:s', strtotime($row['created_on_dtm'])); 
                                            }else{
                                                $created_on_dtm = $row['created_on_dtm'];
                                            }
                                            if ($row['updated_on_dtm'] != null) {
                                                $updated_on_dtm = date('d-m-Y H:i:s', strtotime($row['updated_on_dtm'])); 
                                            }else{
                                                $updated_on_dtm = $row['updated_on_dtm'];
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td><?= $row['fullname_var'] ?></td>
                                                <td><?= $row['phone_number_var'] ?></td>
                                                <td><?= $row['email_var'] ?></td>
                                                <td><?= $row['address_var'] ?></td>
                                                <td class="text-center"><?= $created_on_dtm ?></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="7" class="text-center">Data Tidak Ditemukan</td>
                                        </tr>
                                        <?php
                                    }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
