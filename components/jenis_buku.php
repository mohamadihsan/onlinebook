<?php

// Init Nama Menu 
$menu = 'jenis buku';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();

// JIKA ADA ACTION TAMBAH / UBAH /HAPUS DATA
if (isset($_POST['simpan'])) {
    $nama_jenis_buku_var = mysqli_real_escape_string($conn, $_POST['nama_jenis_buku_var']);
    $deskripsi_jenis_buku_var = mysqli_real_escape_string($conn, $_POST['deskripsi_jenis_buku_var']);

    $sql = "INSERT INTO t_mtr_jenis_buku (nama_jenis_buku_var, deskripsi_jenis_buku_var)
    VALUES ('".$nama_jenis_buku_var."', '".$deskripsi_jenis_buku_var."')";

    if (mysqli_query($conn, $sql)) {
        ?><script>msg_berhasil('Data Jenis Buku telah disimpan...')</script><?php
    } else {
        ?><script>msg_gagal('Data gagal disimpan...! Nama Jenis Buku Sudah Tersedia.')</script><?php
    }
}

if (isset($_POST['ubah'])) {
    $id_jenis_buku_seq = $_POST['id_jenis_buku_seq'];
    $nama_jenis_buku_var = mysqli_real_escape_string($conn, $_POST['nama_jenis_buku_var']);
    $deskripsi_jenis_buku_var = mysqli_real_escape_string($conn, $_POST['deskripsi_jenis_buku_var']);
    $updated_on_dtm = date('Y-m-d H:i:s');

    $sql = "UPDATE t_mtr_jenis_buku 
            SET nama_jenis_buku_var = '".$nama_jenis_buku_var."', 
                deskripsi_jenis_buku_var = '".$deskripsi_jenis_buku_var."',
                updated_on_dtm = '".$updated_on_dtm."'
            WHERE id_jenis_buku_seq = $id_jenis_buku_seq";

    if (mysqli_query($conn, $sql)) {
        ?><script>msg_berhasil('Data Jenis Buku telah diperbaharui...')</script><?php
    } else {
        ?><script>msg_gagal('Data Jenis Buku gagal diperbaharui...')</script><?php
    }
}

if (isset($_POST['hapus'])) {
    $id_jenis_buku_seq = $_POST['id_jenis_buku_seq'];

    $sql = "DELETE FROM t_mtr_jenis_buku WHERE id_jenis_buku_seq = ".$id_jenis_buku_seq."";

    if (mysqli_query($conn, $sql)) {
        ?><script>msg_berhasil('Data Jenis Buku telah dihapus...')</script><?php
    } else {
        ?><script>msg_gagal('Data Jenis Buku Gagal dihapus...')</script><?php
    }
}

?>
<title>Jenis Buku</title>

<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>
                BUKU ONLINE
                <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
            </h2>
        </div> -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            JENIS BUKU
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <!-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul> -->
                                <button type="button" class="btn bg-light-blue waves-effect btn-sm" data-toggle="modal" data-target="#modalTambah">
                                    <i class="material-icons">create</i> <strong>TAMBAH</strong>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Jenis Buku</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Created at</th>
                                        <th class="text-center">Updated at</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT * FROM t_mtr_jenis_buku";
                                    $result = mysqli_query($conn, $sql);
                                
                                    if (mysqli_num_rows($result) > 0) {
                                        $no = 1;
                                        while($row = mysqli_fetch_assoc($result)){
                                            if ($row['created_on_dtm'] != null) {
                                                $created_on_dtm = date('d-m-Y H:i:s', strtotime($row['created_on_dtm'])); 
                                            }else{
                                                $created_on_dtm = $row['created_on_dtm'];
                                            }
                                            if ($row['updated_on_dtm'] != null) {
                                                $updated_on_dtm = date('d-m-Y H:i:s', strtotime($row['updated_on_dtm'])); 
                                            }else{
                                                $updated_on_dtm = $row['updated_on_dtm'];
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center" style="white-space: nowrap">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="material-icons">list</i> 
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a onclick="ubah(<?= $row['id_jenis_buku_seq'] ?>)">
                                                                    <i class="material-icons col-orange">mode_edit</i> Ubah
                                                                </a>
                                                            </li>
                                                            <li role="separator" class="divider"></li>
                                                            <li>
                                                                <a onclick="hapus(<?= $row['id_jenis_buku_seq'] ?>)">
                                                                <i class="material-icons col-red">delete_forever</i> Hapus
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td><?= $row['nama_jenis_buku_var'] ?></td>
                                                <td><?= $row['deskripsi_jenis_buku_var'] ?></td>
                                                <td class="text-center"><?= $created_on_dtm ?></td>
                                                <td class="text-center"><?= $updated_on_dtm ?></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="6" class="text-center">Data Tidak Ditemukan</td>
                                        </tr>
                                        <?php
                                    }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- Modal Tambah Data -->
                        <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalTambahLabel">Tambah Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="nama_jenis_buku_var" maxlength="50" minlength="3" autocomplete="off" required>
                                                    <label class="form-label">Nama Jenis Buku</label>
                                                </div>
                                                <div class="help-info">Min. 3, Max. 50 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="deskripsi_jenis_buku_var" maxlength="255" minlength="0" autocomplete="off">
                                                    <label class="form-label">Keterangan</label>
                                                </div>
                                                <div class="help-info">Min. 0, Max. 255 characters</div>
                                            </div>
                                            <!-- <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="minmaxvalue" min="10" max="200" required>
                                                    <label class="form-label">Min/Max Value</label>
                                                </div>
                                                <div class="help-info">Min. Value: 10, Max. Value: 200</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="url" class="form-control" name="url" required>
                                                    <label class="form-label">Url</label>
                                                </div>
                                                <div class="help-info">Starts with http://, https://, ftp:// etc</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="date" required>
                                                    <label class="form-label">Date</label>
                                                </div>
                                                <div class="help-info">YYYY-MM-DD format</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="number" class="form-control" name="number" required>
                                                    <label class="form-label">Number</label>
                                                </div>
                                                <div class="help-info">Numbers only</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="creditcard" pattern="[0-9]{13,16}" required>
                                                    <label class="form-label">Credit Card</label>
                                                </div>
                                                <div class="help-info">Ex: 1234-5678-9012-3456</div>
                                            </div> -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="simpan" class="btn bg-green btn-link waves-effect">SIMPAN DATA</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Ubah Data -->
                        <div class="modal fade" id="modalUbah" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">

                                        <input type="hidden" name="id_jenis_buku_seq" class="id_jenis_buku_seq" readonly="">

                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalUbahLabel">Ubah Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="nama_jenis_buku_var" id="nama_jenis_buku_var" maxlength="50" minlength="3" autocomplete="off" required autofocus>
                                                    <label class="form-label">Nama Jenis Buku</label>
                                                </div>
                                                <div class="help-info">Min. 3, Max. 50 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="deskripsi_jenis_buku_var" id="deskripsi_jenis_buku_var" maxlength="255" minlength="0" autocomplete="off">
                                                    <label class="form-label">Keterangan</label>
                                                </div>
                                                <div class="help-info">Min. 0, Max. 255 characters</div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="ubah" class="btn bg-green btn-link waves-effect">SIMPAN PERUBAHAN</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Hapus Data -->
                        <div class="modal fade" id="modalHapus" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalHapusLabel">Hapus Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Apakah Anda yakin akan menghapus Data ini?</h5>
                                            <input type="hidden" name="id_jenis_buku_seq" class="id_jenis_buku_seq" readonly="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="hapus" class="btn bg-red btn-link waves-effect">HAPUS DATA</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<script>
    function ubah(id){
        $.ajax({
            url: '../components/api/api_jenis_buku.php',
            type: 'post',
            data: { "id_jenis_buku_seq": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                $('.id_jenis_buku_seq').val(data.id_jenis_buku_seq);
                $('#nama_jenis_buku_var').val(data.nama_jenis_buku_var);
                $('#deskripsi_jenis_buku_var').val(data.deskripsi_jenis_buku_var);        
                $('#modalUbah').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }

    function hapus(id){
        $('.id_jenis_buku_seq').val(id);
        $('#modalHapus').modal('show');
    }
</script>