<?php
    require_once '../global/connection.php';

    $conn = connect_to_server();
    
    $id_buku_int = $_POST['id_buku_int']; 

    $sql = "SELECT * FROM t_mtr_detail_buku WHERE id_buku_int = $id_buku_int";
    $result = mysqli_query($conn, $sql);

    $row = mysqli_fetch_assoc($result);
    $data = array(
        'id_buku_int' => $row['id_buku_int'],
        'nomor_isbn_var' => $row['nomor_isbn_var'],
        'gambar_cover_buku_var' => $row['gambar_cover_buku_var'],
        'penulis_var' => $row['penulis_var'],
        'penerbit_var' => $row['penerbit_var'],
        'tanggal_terbit_var' => $row['tanggal_terbit_var'],
        'deskripsi_buku_txt' => $row['deskripsi_buku_txt'],
        'jumlah_halaman_int' => $row['jumlah_halaman_int'],
        'jenis_bahan_cover_buku_var' => $row['jenis_bahan_cover_buku_var'],
        'dimensi_buku_var' => $row['dimensi_buku_var'],
        'bahasa_var' => $row['bahasa_var'],
        'stok_int' => $row['stok_int'],
        'tersedia_pada_tanggal_dtm' => $row['tersedia_pada_tanggal_dtm'],
        'diskon_int' => $row['diskon_int'],
        'tanggal_mulai_diskon_dtm' => $row['tanggal_mulai_diskon_dtm'],
        'tanggal_akhir_diskon_dtm' => $row['tanggal_akhir_diskon_dtm']
    );

    echo json_encode($data);
?>