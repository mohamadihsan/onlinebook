<?php
    require_once '../global/connection.php';

    $conn = connect_to_server();
    
    $id_buku_seq = $_POST['id_buku_seq']; 

    $sql = "SELECT * FROM t_mtr_buku WHERE id_buku_seq = $id_buku_seq";
    $result = mysqli_query($conn, $sql);

    $row = mysqli_fetch_assoc($result);
    $data = array(
        'id_buku_seq' => $row['id_buku_seq'],
        'nama_buku_var' => $row['nama_buku_var'],
        'id_jenis_buku_int' => $row['id_jenis_buku_int'],
        'status_rekomendasi_boo' => $row['status_rekomendasi_boo']
    );

    echo json_encode($data);
?>