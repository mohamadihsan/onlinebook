<?php
    require_once '../global/connection.php';

    $conn = connect_to_server();
    
    $id_pembelian_int = $_POST['id_pembelian_seq']; 

    $sql = "SELECT * FROM t_trx_validasi_pembayaran WHERE id_pembelian_int = $id_pembelian_int";
    $result = mysqli_query($conn, $sql);

    $row = mysqli_fetch_assoc($result);
    $data = array(
        'id_pembelian_int' => $row['id_pembelian_int'],
        'total_pembelian_int' => $row['total_pembelian_int'],
        'nama_bank_var' => $row['nama_bank_var'],
        'nama_rekening_pembeli_var' => $row['nama_rekening_pembeli_var'],
        'tanggal_pembayaran_dtm' => $row['tanggal_pembayaran_dtm'],
        'status_pembayaran_boo' => $row['status_pembayaran_boo']
    );

    echo json_encode($data);
?>