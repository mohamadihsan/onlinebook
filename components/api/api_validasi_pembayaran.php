<?php
    require_once '../global/connection.php';

    $conn = connect_to_server();
    
    $id_pembelian_seq = $_POST['id_pembelian_seq']; 

    $sql = "SELECT * FROM t_trx_pembelian WHERE id_pembelian_seq = $id_pembelian_seq";
    $result = mysqli_query($conn, $sql);

    $row = mysqli_fetch_assoc($result);
    $data = array(
        'id_pembelian_seq' => $row['id_pembelian_seq'],
        'total_pembelian_int' => $row['total_pembelian_int']
    );

    echo json_encode($data);
?>