<?php
    require_once '../global/connection.php';

    $conn = connect_to_server();
    
    $id_pembelian_seq = $_POST['id_pembelian_seq']; 

    $sql = "SELECT 
                t_trx_detail_pembelian.jumlah_beli_int, 
                t_trx_detail_pembelian.diskon_int,
                t_trx_detail_pembelian.harga_buku_sebelum_diskon_int,
                t_trx_detail_pembelian.harga_buku_sesudah_diskon_int,
                t_trx_detail_pembelian.sub_total_int,
                t_mtr_buku.nama_buku_var 
            FROM t_trx_detail_pembelian 
            LEFT JOIN t_mtr_buku ON t_mtr_buku.id_buku_seq = t_trx_detail_pembelian.id_buku_int
            WHERE t_trx_detail_pembelian.id_pembelian_int = $id_pembelian_seq";
    $result = mysqli_query($conn, $sql);

    while($row = mysqli_fetch_assoc($result)){
        $data[] = array(
            'jumlah_beli_int' => $row['jumlah_beli_int'],
            'diskon_int' => $row['diskon_int'],
            'harga_buku_sebelum_diskon_int' => $row['harga_buku_sebelum_diskon_int'],
            'harga_buku_sesudah_diskon_int' => $row['harga_buku_sesudah_diskon_int'],
            'sub_total_int' => $row['sub_total_int'],
            'nama_buku_var' => $row['nama_buku_var']
        );    
    }
    

    echo json_encode($data);
?>