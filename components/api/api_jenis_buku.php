<?php
    require_once '../global/connection.php';

    $conn = connect_to_server();
    
    $id_jenis_buku_seq = $_POST['id_jenis_buku_seq']; 

    $sql = "SELECT * FROM t_mtr_jenis_buku WHERE id_jenis_buku_seq = $id_jenis_buku_seq";
    $result = mysqli_query($conn, $sql);

    $row = mysqli_fetch_assoc($result);
    $data = array(
        'id_jenis_buku_seq' => $row['id_jenis_buku_seq'],
        'nama_jenis_buku_var' => $row['nama_jenis_buku_var'],
        'deskripsi_jenis_buku_var' => $row['deskripsi_jenis_buku_var']
    );

    echo json_encode($data);
?>