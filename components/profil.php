<?php

// Init Nama Menu 
$menu = 'profil';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();

if (isset($_POST['ubah'])) {
    $id_user_seq = $_SESSION['id_user_seq'];
    $fullname_var = mysqli_real_escape_string($conn, $_POST['fullname_var']);
    $username_var = mysqli_real_escape_string($conn, $_POST['username_var']);
    $password_var = mysqli_real_escape_string($conn, md5($_POST['password_var']));
    $updated_on_dtm = date('Y-m-d H:i:s');

    $sql = "UPDATE t_mtr_user 
            SET username_var = '".$username_var."', 
                password_var = '".$password_var."',
                updated_on_dtm = '".$updated_on_dtm."'
            WHERE id_user_seq = $id_user_seq";

    if (mysqli_query($conn, $sql)) {

        $sql = "UPDATE t_mtr_detail_user 
                SET fullname_var = '".$fullname_var."',
                    updated_on_dtm = '".$updated_on_dtm."'
                WHERE id_user_int = $id_user_seq";
        mysqli_query($conn, $sql);                

        ?><script>msg_berhasil('Akun Anda telah diperbaharui...')</script><?php
    } else {
        ?><script>msg_gagal('Akun Anda gagal diperbaharui...')</script><?php
    }
}

?>
<title>Profil</title>

<section class="content">
    <div class="container-fluid">
        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            PROFIL
                        </h2>
                    </div>
                    <div class="body">
                        
                        <?php
                        $id_user_seq = $_SESSION['id_user_seq'];
                        $username_var = $_SESSION['username_var'];
                        $fullname_var = $_SESSION['fullname_var'];
                        ?>
                        <form action="" id="form_advanced_validation" method="post">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="fullname_var" maxlength="80" minlength="3" autocomplete="off" value="<?= $fullname_var ?>" required>
                                    <label class="form-label">Nama Lengkap</label>
                                </div>
                                <div class="help-info">Min. 3, Max. 80 characters</div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="username_var" maxlength="255" minlength="3" autocomplete="off" value="<?= $username_var ?>" required>
                                    <label class="form-label">Username</label>
                                </div>
                                <div class="help-info">Min. 3 characters</div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password_var" maxlength="255" minlength="3" autocomplete="off" required>
                                    <label class="form-label">Password</label>
                                </div>
                                <div class="help-info">Min. 3 characters</div>
                            </div>
                            <button type="submit" name="ubah" class="btn bg-green btn-link waves-effect">SIMPAN DATA</button>
                        </form>

                                
                        
                        <!-- Modal Tambah Data -->
                        <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalTambahLabel">Tambah Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="nama_jenis_buku_var" maxlength="50" minlength="3" autocomplete="off" required>
                                                    <label class="form-label">Nama Jenis Buku</label>
                                                </div>
                                                <div class="help-info">Min. 3, Max. 50 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="deskripsi_jenis_buku_var" maxlength="255" minlength="0" autocomplete="off">
                                                    <label class="form-label">Keterangan</label>
                                                </div>
                                                <div class="help-info">Min. 0, Max. 255 characters</div>
                                            </div>
                                            <!-- <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="minmaxvalue" min="10" max="200" required>
                                                    <label class="form-label">Min/Max Value</label>
                                                </div>
                                                <div class="help-info">Min. Value: 10, Max. Value: 200</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="url" class="form-control" name="url" required>
                                                    <label class="form-label">Url</label>
                                                </div>
                                                <div class="help-info">Starts with http://, https://, ftp:// etc</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="date" required>
                                                    <label class="form-label">Date</label>
                                                </div>
                                                <div class="help-info">YYYY-MM-DD format</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="number" class="form-control" name="number" required>
                                                    <label class="form-label">Number</label>
                                                </div>
                                                <div class="help-info">Numbers only</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="creditcard" pattern="[0-9]{13,16}" required>
                                                    <label class="form-label">Credit Card</label>
                                                </div>
                                                <div class="help-info">Ex: 1234-5678-9012-3456</div>
                                            </div> -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="simpan" class="btn bg-green btn-link waves-effect">SIMPAN DATA</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Ubah Data -->
                        <div class="modal fade" id="modalUbah" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">

                                        <input type="hidden" name="id_jenis_buku_seq" class="id_jenis_buku_seq" readonly="">

                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalUbahLabel">Ubah Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="nama_jenis_buku_var" id="nama_jenis_buku_var" maxlength="50" minlength="3" autocomplete="off" required autofocus>
                                                    <label class="form-label">Nama Jenis Buku</label>
                                                </div>
                                                <div class="help-info">Min. 3, Max. 50 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="deskripsi_jenis_buku_var" id="deskripsi_jenis_buku_var" maxlength="255" minlength="0" autocomplete="off">
                                                    <label class="form-label">Keterangan</label>
                                                </div>
                                                <div class="help-info">Min. 0, Max. 255 characters</div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="ubah" class="btn bg-green btn-link waves-effect">SIMPAN PERUBAHAN</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Hapus Data -->
                        <div class="modal fade" id="modalHapus" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalHapusLabel">Hapus Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Apakah Anda yakin akan menghapus Data ini?</h5>
                                            <input type="hidden" name="id_jenis_buku_seq" class="id_jenis_buku_seq" readonly="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="hapus" class="btn bg-red btn-link waves-effect">HAPUS DATA</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<script>
    function ubah(id){
        $.ajax({
            url: '../components/api/api_jenis_buku.php',
            type: 'post',
            data: { "id_jenis_buku_seq": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                $('.id_jenis_buku_seq').val(data.id_jenis_buku_seq);
                $('#nama_jenis_buku_var').val(data.nama_jenis_buku_var);
                $('#deskripsi_jenis_buku_var').val(data.deskripsi_jenis_buku_var);        
                $('#modalUbah').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }

    function hapus(id){
        $('.id_jenis_buku_seq').val(id);
        $('#modalHapus').modal('show');
    }
</script>