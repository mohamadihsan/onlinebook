<?php

// Init Nama Menu 
$menu = 'beranda';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();

$sql = "SELECT * FROM t_trx_pembelian WHERE status_pembayaran_boo = 1";
$result = mysqli_query($conn, $sql);
$total_penjualan = mysqli_num_rows($result);

$sql = "SELECT * FROM t_mtr_buku";
$result = mysqli_query($conn, $sql);
$total_buku = mysqli_num_rows($result);

$sql = "SELECT * FROM t_mtr_jenis_buku";
$result = mysqli_query($conn, $sql);
$total_jenis_buku = mysqli_num_rows($result);

$sql = "SELECT * FROM t_trx_pembelian WHERE status_pembayaran_boo = 0";
$result = mysqli_query($conn, $sql);
$total_belum_melakukan_pembayaran = mysqli_num_rows($result);
?>
<title>Beranda</title>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>BERANDA</h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <div class="icon bg-red">
                        <i class="material-icons">shopping_cart</i>
                    </div>
                    <div class="content">
                        <div class="text">PEMBELIAN</div>
                        <div class="number count-to" data-from="0" data-to="<?= $total_penjualan ?>" data-speed="1000" data-fresh-interval="20"><?= $total_penjualan ?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <div class="icon bg-indigo">
                        <i class="material-icons">book</i>
                    </div>
                    <div class="content">
                        <div class="text">BUKU</div>
                        <div class="number count-to" data-from="0" data-to="<?= $total_buku ?>" data-speed="1000" data-fresh-interval="20"><?= $total_buku ?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <div class="icon bg-purple">
                        <i class="material-icons">list</i>
                    </div>
                    <div class="content">
                        <div class="text">JENIS BUKU</div>
                        <div class="number count-to" data-from="0" data-to="<?= $total_jenis_buku ?>" data-speed="1000" data-fresh-interval="20"><?= $total_jenis_buku ?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <div class="icon bg-deep-purple">
                        <i class="material-icons">account_balance_wallet</i>
                    </div>
                    <div class="content">
                        <div class="text">BELUM BAYAR</div>
                        <div class="number count-to" data-from="0" data-to="<?= $total_belum_melakukan_pembayaran ?>" data-speed="1500" data-fresh-interval="20"><?= $total_belum_melakukan_pembayaran ?></div>
                    </div>
                </div>
            </div>

        	<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="../assets/images/image-gallery/11.jpg" style="height: 10%" />
                        </div>
                        <div class="item">
                            <img src="../assets/images/image-gallery/12.jpg" />
                        </div>
                        <div class="item">
                            <img src="../assets/images/image-gallery/19.jpg" />
                        </div>
                    </div>

                    
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div> -->
        </div>
    </div>
</section>