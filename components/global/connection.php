<?php  
function connect_to_server()
{
	$servername 	= "localhost";
	$username 		= "root";
	$password 		= "";
	$databasename	= "db_onlinebook";

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $databasename);

	// Check connection
	if (!$conn) {
	    return false;
	}
	return $conn;
}
?>