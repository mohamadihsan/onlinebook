<?php  
function check_login($conn, $data)
{
	$sql = "SELECT  
				t_mtr_user.id_user_seq,
				t_mtr_user.username_var,
				t_mtr_detail_user.fullname_var,
				t_mtr_user.id_grup_user_int,
				t_mtr_grup_user.nama_grup_user_var
			FROM t_mtr_user
			LEFT JOIN t_mtr_detail_user ON t_mtr_detail_user.id_user_int = t_mtr_user.id_user_seq
			LEFT JOIN t_mtr_grup_user ON t_mtr_grup_user.id_grup_user_seq = t_mtr_user.id_grup_user_int
			WHERE
				t_mtr_user.username_var = '".$data['username_var']."' AND
				t_mtr_user.password_var = '".$data['password_var']."' AND
				t_mtr_user.status_aktif_boo = 1 
			";
	$result = mysqli_query($conn, $sql);

	if (mysqli_num_rows($result) > 0) {
		$token_var = randomString(50);

	    $row = mysqli_fetch_assoc($result);
    	$_SESSION['id_user_seq'] 		= $row['id_user_seq'];
    	$_SESSION['username_var'] 		= $row['username_var'];
    	$_SESSION['fullname_var'] 		= $row['fullname_var'];
    	$_SESSION['id_grup_user_int']	= $row['id_grup_user_int'];
    	$_SESSION['nama_grup_user_var']	= $row['nama_grup_user_var'];
    	$_SESSION['token_var']			= $token_var;

		return $token_var;	
	} 

	return false;
}

function status_login($conn, $data)
{

	if ($data['token_var'] != null AND $data['username_var'] != null) {
		
		$sql = "SELECT *
			FROM t_mtr_user
			WHERE
				t_mtr_user.username_var = '".$data['username_var']."' AND
				t_mtr_user.token_var = '".$data['token_var']."' AND
				t_mtr_user.status_aktif_boo = 1 
			";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0) {

			return true;	
		} 

		return false;

	}else{

		return false;

	}
}

function update_token($conn, $data)
{
	$sql = "UPDATE t_mtr_user 
			SET t_mtr_user.token_var = '".$data['token_var']."' 
			WHERE t_mtr_user.username_var = '".$data['username_var']."'";

	if (mysqli_query($conn, $sql)) {
	    return true;
	} else {
	    return false;
	}
}
?>