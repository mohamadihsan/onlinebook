<?php  
// Load Modul 
require_once('../components/global/authentication.php');
require_once('../components/global/connection.php');
require_once('../components/global/function.php');

// Cek Koneksi ke Server
$connection_status = connect_to_server();
if ($connection_status == false) {
	
	// munculkan halaman gagal terhubung ke server
	movePage(301, '../components/error/connection_failed.php');exit();
}

// Buat Session
session_start();
$conn = $connection_status;

// Cek Session Login User
$token_var = isset($_SESSION['token_var']) ? $_SESSION['token_var'] : null;
$username_var = isset($_SESSION['username_var']) ? $_SESSION['username_var'] : null;

$data = array(
	'token_var'		=> $token_var,
	'username_var'	=> $username_var
);

// var_dump($data);die();
// Cek Status Login
$status_login = status_login($conn, $data);
if ($status_login == true AND $menu != 'beranda') {
	
	// $list_menu = list_menu();

	// foreach ($list_menu as $lm) {
		
	// 	$menu = strtolower($menu);
	// 	$nama_menu_var = strtolower($lm['nama_menu_var']);
	// 	$index_nama_file_var = $lm['index_nama_file_var'];

	// 	if ($menu == $nama_menu_var) {
	// 		movePage(301, $index_nama_file_var);exit();
	// 	}         
	// }

	// Munculkan halaman beranda
	// movePage(301, '../components/beranda.php');exit();
}else if ($status_login == false AND $menu != 'login') {

	// Default Munculkan halaman Login
	movePage(301, '../components/login.php');exit();
}
?>