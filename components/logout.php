<?php  
// Load Modul
require_once('../components/global/function.php');

session_start();
if(session_destroy()){

	// Munculkan halaman Login
	movePage(301, '../admin/index.php');

}
?>