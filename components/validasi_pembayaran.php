<?php

// Init Nama Menu 
$menu = 'validasi pembayaran';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/global/function.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();

if (isset($_POST['konfirmasi'])) {
    $id_pembelian_seq = $_POST['id_pembelian_seq'];
    $total_pembelian_int = $_POST['total_pembelian_int'];
    $nama_bank_var = mysqli_real_escape_string($conn, $_POST['nama_bank_var']);
    $nama_rekening_pembeli_var = mysqli_real_escape_string($conn, $_POST['nama_rekening_pembeli_var']);
    $nomor_rekening_var = mysqli_real_escape_string($conn, $_POST['nomor_rekening_var']);
    $tanggal_pembayaran_dtm = mysqli_real_escape_string($conn, date('Y-m-d', strtotime($_POST['tanggal_pembayaran_dtm'])));
    $tanggal_validasi_pembayaran_dtm = date('Y-m-d H:i:s');
    // $di_validasi_oleh_int = $_SESSION['id_user_int'];
    $di_validasi_oleh_int = 1;
    $status_pembayaran_boo = 1;

    $sql = "INSERT INTO t_trx_validasi_pembayaran 
            (
                id_pembelian_int, 
                total_pembelian_int, 
                nama_bank_var, 
                nama_rekening_pembeli_var, 
                nomor_rekening_var, 
                tanggal_pembayaran_dtm, 
                di_validasi_oleh_int, 
                status_pembayaran_boo
            )
    VALUES (
            ".$id_pembelian_seq.", 
            ".$total_pembelian_int.",
            '".$nama_bank_var."',
            '".$nama_rekening_pembeli_var."',
            '".$nomor_rekening_var."',
            '".$tanggal_pembayaran_dtm."',
            ".$di_validasi_oleh_int.",
            ".$status_pembayaran_boo."
            )";

    if (mysqli_query($conn, $sql)) {

        // update
        $sql = "UPDATE t_trx_pembelian
        SET status_pembayaran_boo = ".$status_pembayaran_boo.", 
            id_user_int = ".$di_validasi_oleh_int.",
            tanggal_validasi_pembayaran_dtm = '".$tanggal_validasi_pembayaran_dtm."'
        WHERE id_pembelian_seq = $id_pembelian_seq";

        mysqli_query($conn, $sql);
        ?><script>msg_berhasil('Data pembelian telah divalidasi! Status Pembayaran telah diterima...')</script><?php
    } else {
        echo mysqli_error($conn);
        ?><script>msg_gagal('Gagal memvalidasi status pembayaran...')</script><?php
    }
}

?>
<title>Validasi Pembayaran</title>

<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>
                BUKU ONLINE
                <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
            </h2>
        </div> -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Validasi Pembayaran
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <!-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul> -->
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Invoice</th>
                                        <th class="text-center">Customer</th>
                                        <th class="text-center">Batas Pembayaran</th>
                                        <th class="text-center">Total Pembelian</th>
                                        <th class="text-center">Tanggal Pembayaran</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT 
                                                t_trx_pembelian.id_pembelian_seq,
                                                t_trx_pembelian.invoice_pembelian_var,
                                                t_trx_pembelian.total_pembelian_int,
                                                t_trx_pembelian.status_pembayaran_boo,
                                                t_trx_pembelian.batas_akhir_pembayaran_dtm,
                                                t_trx_pembelian.tanggal_validasi_pembayaran_dtm,
                                                t_mtr_detail_user.fullname_var 
                                            FROM t_trx_pembelian
                                            LEFT JOIN t_mtr_user ON t_mtr_user.id_user_seq = t_trx_pembelian.id_user_int
                                            LEFT JOIN t_mtr_detail_user ON t_mtr_detail_user.id_user_int = t_mtr_user.id_user_seq
                                            ORDER BY t_trx_pembelian.status_pembayaran_boo ASC, t_trx_pembelian.id_pembelian_seq DESC";
                                    $result = mysqli_query($conn, $sql);
                                
                                    if (mysqli_num_rows($result) > 0) {
                                        $no = 1;
                                        while($row = mysqli_fetch_assoc($result)){
                                            if ($row['status_pembayaran_boo'] == 0) {
                                                $status_pembayaran_boo = '<span class="label bg-red">Belum Dibayar</span>';
                                            }else{
                                                $status_pembayaran_boo = '<span class="label bg-green">Sudah Dibayar</span>';
                                            }
                                            $batas_akhir_pembayaran_dtm = tanggal_indo(date('d-m-Y', strtotime($row['batas_akhir_pembayaran_dtm'])));

                                            if ($row['tanggal_validasi_pembayaran_dtm'] != null) {
                                                $tanggal_validasi_pembayaran_dtm = tanggal_indo(date('d-m-Y', strtotime($row['tanggal_validasi_pembayaran_dtm'])));    
                                            }else{
                                                $tanggal_validasi_pembayaran_dtm = '';
                                            }
                                            
                                            ?>
                                            <tr>
                                                <td class="text-center" style="white-space: nowrap">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="material-icons">list</i> 
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            if ($row['status_pembayaran_boo'] == 0) { 
                                                                ?>
                                                                <li>
                                                                    <a onclick="ubah(<?= $row['id_pembelian_seq'] ?>)">
                                                                        <i class="material-icons col-green">attach_money</i> Validasi
                                                                    </a>
                                                                </li>
                                                                <?php
                                                            }
                                                            if ($row['status_pembayaran_boo'] == 1) { 
                                                                ?>
                                                                <li role="separator" class="divider"></li>
                                                                <li>
                                                                    <a onclick="detail_pembayaran(<?= $row['id_pembelian_seq'] ?>)">
                                                                    <i class="material-icons col-blue">chrome_reader_mode</i> Detail
                                                                    </a>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td><?= $row['invoice_pembelian_var'] ?></td>
                                                <td><?= $row['fullname_var'] ?></td>
                                                <td class="text-center"><?= $batas_akhir_pembayaran_dtm ?></td>
                                                <td class="text-right"><?= rupiah($row['total_pembelian_int']) ?></td>
                                                <td class="text-center"><?= $tanggal_validasi_pembayaran_dtm ?></td>
                                                <td class="text-center"><?= $status_pembayaran_boo ?></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="7" class="text-center">Data Tidak Ditemukan</td>
                                        </tr>
                                        <?php
                                    }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                                    
                        <!-- Modal Validasi Data -->
                        <div class="modal fade" id="modalValidasi" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">

                                        <input type="hidden" name="id_pembelian_seq" class="id_pembelian_seq" readonly="">

                                        <input type="hidden" class="total_pembelian_int" name="total_pembelian_int" placeholder="" id="total_pembelian_int" readonly="">

                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalValidasiLabel">Validasi Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <span id="total_pembelian_int_label"></span>
                                                <div class="form-line">
                                                    <label class="">Tanggal Pembayaran :</label>
                                                    <div class="form-line">
                                                        <input type="text" class="datepicker form-control" name="tanggal_pembayaran_dtm" placeholder="" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Nama Bank Pengirim</label>
                                                    <input type="text" class="form-control" name="nama_bank_var" maxlength="50" minlength="0" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Nomor Rekening Pengirim</label>
                                                    <input type="text" class="form-control" name="nomor_rekening_var" maxlength="50" minlength="0" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Nama Rekening Pengirim</label>
                                                    <input type="text" class="form-control" name="nama_rekening_pembeli_var" maxlength="50" minlength="0" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="konfirmasi" class="btn bg-green btn-link waves-effect">SIMPAN</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Detail Pembayaran Data -->
                        <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalDetailLabel">Detail Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            <span id="detail_pembayaran"></span>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<script>
    function ubah(id){
        $.ajax({
            url: '../components/api/api_validasi_pembayaran.php',
            type: 'post',
            data: { "id_pembelian_seq": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                $('.id_pembelian_seq').val(data.id_pembelian_seq);  
                $('#total_pembelian_int').val(data.total_pembelian_int);   
                $('#total_pembelian_int_label').html('<h5>Total Pembelian / Total Tagihan : Rp.'+data.total_pembelian_int+'</h5>');        
                $('#modalValidasi').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }

    function detail_pembayaran(id){
        $.ajax({
            url: '../components/api/api_detail_pembayaran.php',
            type: 'post',
            data: { "id_pembelian_seq": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                
                var detail = 
                    '<table class="table table-stripped">'+
                        '<tr>'+
                            '<td class="text-bold">Nama Bank Pengirim</td>'+
                            '<td>: '+data.nama_bank_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Nama Rekening Pengirim</td>'+
                            '<td>: '+data.nama_rekening_pembeli_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Nominal Pembayaran</td>'+
                            '<td>: '+data.total_pembelian_int+'</td>'+
                        '</tr>'+
                    '</table>';
                $('#detail_pembayaran').html(detail);
                $('#modalDetail').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }
</script>