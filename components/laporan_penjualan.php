<?php

// Init Nama Menu 
$menu = 'laporan penjualan';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/global/function.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();

?>
<title>Laporan Penjualan</title>

<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>
                BUKU ONLINE
                <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
            </h2>
        </div> -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Laporan Penjualan
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <!-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul> -->
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <form action="" method="post">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="">Filter</label>
                                            <input type="text" class="datepicker form-control" name="start_date" id="start_date" autocomplete="off" placeholder="Pilih Tanggal" required>
                                        </div>
                                        <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                    </div>    
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="">.</label>
                                            <input type="text" class="datepicker form-control" name="end_date" id="end_date" autocomplete="off" placeholder="Pilih Tanggal" required>
                                        </div>
                                        <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                    </div>    
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <button type="submit" name="filter" class="btn btn-sm">Filter</button>
                                </div>
                            </form><hr>
                            <br/><br/><br/><br/>

                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <?php  
                                if (isset($_POST['filter'])) {
                                        
                                    $start_date = mysqli_real_escape_string($conn, date('Y-m-d', strtotime($_POST['start_date'])));
                                    $end_date = mysqli_real_escape_string($conn, date('Y-m-d', strtotime($_POST['end_date'])));

                                    $where = "WHERE t_trx_pembelian.created_on_dtm BETWEEN '".$start_date."' AND '".$end_date."'";

                                    $title = '<h4 class="text-center">Laporan Penjualan Tanggal '.tanggal_indo($start_date).' - '.tanggal_indo($end_date).' </h4><hr>';
                                }else{
                                    $where = '';
                                    $title = '';
                                }

                                echo $title;
                                ?>
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Buku</th>
                                        <th class="text-center">Kategori</th>
                                        <th class="text-center">Jml Penjualan</th>
                                        <th class="text-center">Total Pendapatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    $sql = "SELECT t_mtr_buku.nama_buku_var, t_mtr_jenis_buku.nama_jenis_buku_var, SUM(t_trx_detail_pembelian.jumlah_beli_int) as total_penjualan, SUM(t_trx_detail_pembelian.sub_total_int) as total_pendapatan
										FROM t_mtr_buku
										LEFT JOIN t_mtr_jenis_buku ON t_mtr_buku.id_jenis_buku_int = t_mtr_jenis_buku.id_jenis_buku_seq 
										LEFT JOIN t_trx_detail_pembelian ON t_trx_detail_pembelian.id_buku_int = t_mtr_buku.id_buku_seq 
										LEFT JOIN t_trx_pembelian ON t_trx_pembelian.id_pembelian_seq = t_trx_detail_pembelian.id_pembelian_int
										".$where."
										GROUP BY t_mtr_buku.nama_buku_var, t_mtr_jenis_buku.nama_jenis_buku_var";
                                    $result = mysqli_query($conn, $sql);
                                
                                    if (mysqli_num_rows($result) > 0) {
                                        $no = 1;
                                        while($row = mysqli_fetch_assoc($result)){
                                            ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td><?= $row['nama_buku_var'] ?></td>
                                                <td><?= $row['nama_jenis_buku_var'] ?></td>
                                                <td class="text-right"><?= $row['total_penjualan'] ?></td>
                                                <td class="text-right"><?= rupiah($row['total_pendapatan']) ?></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="5" class="text-center">Data Tidak Ditemukan</td>
                                        </tr>
                                        <?php
                                    }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>