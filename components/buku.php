<?php

// Init Nama Menu 
$menu = 'buku';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();

// JIKA ADA ACTION TAMBAH / UBAH /HAPUS DATA
if (isset($_POST['simpan'])) {
    $nama_buku_var = mysqli_real_escape_string($conn, $_POST['nama_buku_var']);
    $id_jenis_buku_int = $_POST['id_jenis_buku_int'];
    $status_rekomendasi_boo = mysqli_real_escape_string($conn, $_POST['status_rekomendasi_boo']);
    $harga_int = $_POST['harga_int'];

    if($status_rekomendasi_boo != 1){
        $status_rekomendasi_boo = 0;
    }

    $sql = "INSERT INTO t_mtr_buku (nama_buku_var, id_jenis_buku_int, status_rekomendasi_boo, $harga_int)
    VALUES ('".$nama_buku_var."', ".$id_jenis_buku_int.", ".$status_rekomendasi_boo.", ".$harga_int.")";

    if (mysqli_query($conn, $sql)) {
        $id_buku_int = mysqli_insert_id($conn);
        
        $sql = "INSERT INTO t_mtr_detail_buku (id_buku_int) VALUES (".$id_buku_int.")";
        mysqli_query($conn, $sql);
        ?><script>msg_berhasil('Data Buku telah disimpan...')</script><?php
    } else {
        ?><script>msg_gagal('Data gagal disimpan...!')</script><?php
    }
}

if (isset($_POST['ubah'])) {
    $id_buku_seq = $_POST['id_buku_seq'];
    $nama_buku_var = mysqli_real_escape_string($conn, $_POST['nama_buku_var']);
    $id_jenis_buku_int = $_POST['id_jenis_buku_int'];
    $status_rekomendasi_boo = mysqli_real_escape_string($conn, $_POST['status_rekomendasi_boo']);
    $harga_int = $_POST['harga_int'];
    $updated_on_dtm = date('Y-m-d H:i:s');

    if($status_rekomendasi_boo != 1){
        $status_rekomendasi_boo = 0;
    }

    $sql = "UPDATE t_mtr_buku 
            SET nama_buku_var = '".$nama_buku_var."', 
                id_jenis_buku_int = '".$id_jenis_buku_int."',
                status_rekomendasi_boo = ".$status_rekomendasi_boo.",
                harga_int = ".$harga_int.",
                updated_on_dtm = '".$updated_on_dtm."'
            WHERE id_buku_seq = $id_buku_seq";

    if (mysqli_query($conn, $sql)) {
        ?><script>msg_berhasil('Data Buku telah diperbaharui...')</script><?php
    } else {
        ?><script>msg_gagal('Data Buku gagal diperbaharui...')</script><?php
    }
}

if (isset($_POST['ubah_detail'])) {
    $id_buku_int = (int)$_POST['id_buku_int'];
    $nomor_isbn_var = mysqli_real_escape_string($conn, $_POST['nomor_isbn_var']);
    $penulis_var = mysqli_real_escape_string($conn, $_POST['penulis_var']);
    $penerbit_var = mysqli_real_escape_string($conn, $_POST['penerbit_var']);
    $tanggal_terbit_var = mysqli_real_escape_string($conn, $_POST['tanggal_terbit_var']);
    $deskripsi_buku_txt = mysqli_real_escape_string($conn, $_POST['deskripsi_buku_txt']);
    $jumlah_halaman_int = (int)$_POST['jumlah_halaman_int'];
    $jenis_bahan_cover_buku_var = mysqli_real_escape_string($conn, $_POST['jenis_bahan_cover_buku_var']);
    $dimensi_buku_var = mysqli_real_escape_string($conn, $_POST['dimensi_buku_var']);
    $bahasa_var = mysqli_real_escape_string($conn, $_POST['bahasa_var']);
    $stok_int = (int)$_POST['stok_int'];
    $tersedia_pada_tanggal_dtm = mysqli_real_escape_string($conn, $_POST['tersedia_pada_tanggal_dtm']);
    $diskon_int = (int)$_POST['diskon_int'];
    $tanggal_mulai_diskon_dtm = mysqli_real_escape_string($conn, $_POST['tanggal_mulai_diskon_dtm']);
    $tanggal_akhir_diskon_dtm = mysqli_real_escape_string($conn, $_POST['tanggal_akhir_diskon_dtm']);
    $updated_on_dtm = date('Y-m-d H:i:s');

    if($tersedia_pada_tanggal_dtm == ''){
        $tersedia_pada_tanggal_dtm = null;
    }else{
        $tersedia_pada_tanggal_dtm = date('Y-m-d', strtotime($tersedia_pada_tanggal_dtm));
    }
    if($tanggal_mulai_diskon_dtm == ''){
        $tanggal_mulai_diskon_dtm = null;
    }else{
        $tanggal_mulai_diskon_dtm = date('Y-m-d', strtotime($tanggal_mulai_diskon_dtm));
    }
    if($tanggal_akhir_diskon_dtm == ''){
        $tanggal_akhir_diskon_dtm = null;
    }else{
        $tanggal_akhir_diskon_dtm = date('Y-m-d', strtotime($tanggal_akhir_diskon_dtm));
    }

    // $data = array(
    //     'id_buku_int' => $id_buku_int, 
    //     'nomor_isbn_var' => $nomor_isbn_var,  
    //     'penulis_var' => $penulis_var,  
    //     'penerbit_var' => $penerbit_var,  
    //     'tanggal_terbit_var' => $tanggal_terbit_var,  
    //     'deskripsi_buku_txt' => $deskripsi_buku_txt,  
    //     'jumlah_halaman_int' => $jumlah_halaman_int,  
    //     'jenis_bahan_cover_buku_var' => $jenis_bahan_cover_buku_var,  
    //     'dimensi_buku_var' => $dimensi_buku_var,  
    //     'bahasa_var' => $bahasa_var,  
    //     'stok_int' => $stok_int, 
    //     'tersedia_pada_tanggal_dtm' => $tersedia_pada_tanggal_dtm,  
    //     'diskon_int' => $diskon_int, 
    //     'tanggal_mulai_diskon_dtm' => $tanggal_mulai_diskon_dtm,  
    //     'tanggal_akhir_diskon_dtm' => $tanggal_akhir_diskon_dtm,  
    //     'updated_on_dtm' => $updated_on_dtm
    // );
    // var_dump($data);die();

    $sql = "UPDATE t_mtr_detail_buku 
            SET nomor_isbn_var = '".$nomor_isbn_var."',
                penulis_var = '".$penulis_var."',
                penerbit_var = '".$penerbit_var."',
                tanggal_terbit_var = '".$tanggal_terbit_var."',
                deskripsi_buku_txt = '".$deskripsi_buku_txt."',
                jumlah_halaman_int = ".$jumlah_halaman_int.",
                jenis_bahan_cover_buku_var = '".$jenis_bahan_cover_buku_var."',
                dimensi_buku_var = '".$dimensi_buku_var."',
                bahasa_var = '".$bahasa_var."',
                stok_int = ".$stok_int.",
                tersedia_pada_tanggal_dtm = ".($tersedia_pada_tanggal_dtm==NULL ? "NULL" : "'$tersedia_pada_tanggal_dtm'").",
                diskon_int = ".$diskon_int.",
                tanggal_mulai_diskon_dtm = ".($tanggal_mulai_diskon_dtm==NULL ? "NULL" : "'$tanggal_mulai_diskon_dtm'").",
                tanggal_akhir_diskon_dtm = ".($tanggal_akhir_diskon_dtm==NULL ? "NULL" : "'$tanggal_akhir_diskon_dtm'").",
                updated_on_dtm = '".$updated_on_dtm."'
            WHERE id_buku_int = $id_buku_int";

    if (mysqli_query($conn, $sql)) {
        ?><script>msg_berhasil('Detail Buku telah diperbaharui...')</script><?php
    } else {
        echo mysqli_error($conn);
        ?><script>msg_gagal('Detail Buku gagal diperbaharui...')</script><?php
    }
}

if (isset($_POST['hapus'])) {
    $id_buku_seq = $_POST['id_buku_seq'];

    $sql = "DELETE FROM t_mtr_buku WHERE id_buku_seq = ".$id_buku_seq."";

    if (mysqli_query($conn, $sql)) {
        ?><script>msg_berhasil('Data Buku telah dihapus...')</script><?php
    } else {
        ?><script>msg_gagal('Data Buku Gagal dihapus...')</script><?php
    }
}

?>
<title>Buku</title>

<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>
                BUKU ONLINE
                <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
            </h2>
        </div> -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            BUKU
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <!-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul> -->
                                <button type="button" class="btn bg-light-blue waves-effect btn-sm" data-toggle="modal" data-target="#modalTambah">
                                    <i class="material-icons">create</i> <strong>TAMBAH</strong>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Buku</th>
                                        <th class="text-center">Kategori</th>
                                        <th class="text-center">Rekomendasi</th>
                                        <th class="text-center">Harga</th>
                                        <th class="text-center">Created at</th>
                                        <th class="text-center">Updated at</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT * FROM t_mtr_buku
                                            LEFT JOIN t_mtr_jenis_buku ON id_jenis_buku_seq = id_jenis_buku_int";
                                    $result = mysqli_query($conn, $sql);
                                
                                    if (mysqli_num_rows($result) > 0) {
                                        $no = 1;
                                        while($row = mysqli_fetch_assoc($result)){
                                            if($row['status_rekomendasi_boo'] == 1){
                                                $status_rekomendasi_boo = 'YA';
                                            }else{
                                                $status_rekomendasi_boo = 'TIDAK';
                                            }
                                            if ($row['created_on_dtm'] != null) {
                                                $created_on_dtm = date('d-m-Y H:i:s', strtotime($row['created_on_dtm'])); 
                                            }else{
                                                $created_on_dtm = $row['created_on_dtm'];
                                            }
                                            if ($row['updated_on_dtm'] != null) {
                                                $updated_on_dtm = date('d-m-Y H:i:s', strtotime($row['updated_on_dtm'])); 
                                            }else{
                                                $updated_on_dtm = $row['updated_on_dtm'];
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center" style="white-space: nowrap">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="material-icons">list</i> 
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a onclick="ubah_cover(<?= $row['id_buku_seq'] ?>)">
                                                                    <i class="material-icons col-green">add</i> Cover Buku
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onclick="ubah(<?= $row['id_buku_seq'] ?>)">
                                                                    <i class="material-icons col-orange">mode_edit</i> Ubah
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onclick="ubah_detail(<?= $row['id_buku_seq'] ?>)">
                                                                    <i class="material-icons col-orange">edit</i> Detail
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onclick="ubah_detail_view(<?= $row['id_buku_seq'] ?>)">
                                                                    <i class="material-icons col-blue">chrome_reader_mode</i> Detail
                                                                </a>
                                                            </li>
                                                            <li role="separator" class="divider"></li>
                                                            <li>
                                                                <a onclick="hapus(<?= $row['id_buku_seq'] ?>)">
                                                                <i class="material-icons col-red">delete_forever</i> Hapus
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td><?= $row['nama_buku_var'] ?></td>
                                                <td><?= $row['nama_jenis_buku_var'] ?></td>
                                                <td class="text-center"><?= $status_rekomendasi_boo ?></td>
                                                <td class="text-center">Rp.<?= $row['harga_int'] ?></td>
                                                <td class="text-center"><?= $created_on_dtm ?></td>
                                                <td class="text-center"><?= $updated_on_dtm ?></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="7" class="text-center">Data Tidak Ditemukan</td>
                                        </tr>
                                        <?php
                                    }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- Modal Tambah Data -->
                        <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalTambahLabel">Tambah Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="nama_buku_var" maxlength="255" minlength="2" autocomplete="off">
                                                    <label class="form-label">Nama Buku</label>
                                                </div>
                                                <div class="help-info">Min. 2, Max. 255 characters</div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Kategori Buku</label>
                                                <select class="form-control show-tick" name="id_jenis_buku_int">
                                                    <?php 
                                                    $sql = "SELECT * FROM t_mtr_jenis_buku";
                                                    $result = mysqli_query($conn, $sql);
                                                
                                                    while($row = mysqli_fetch_assoc($result)){
                                                        ?>
                                                        <option value="<?= $row['id_jenis_buku_seq'] ?>"><?= $row['nama_jenis_buku_var'] ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="number" class="form-control" name="harga_int" min="0" value="0" autocomplete="off">
                                                    <label class="form-label">Harga Buku</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <span class="">
                                                    <input type="hidden" name="status_rekomendasi_boo" value="0" />
                                                    <input type="checkbox" class="filled-in" name="status_rekomendasi_boo" value="1">
                                                    <label for="status_rekomendasi_boo">Direkomendasi</label>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="simpan" class="btn bg-green btn-link waves-effect">SIMPAN DATA</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Ubah Data -->
                        <div class="modal fade" id="modalUbah" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">

                                        <input type="hidden" name="id_buku_seq" class="id_buku_seq" readonly="">

                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalUbahLabel">Ubah Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="nama_buku_var" id="nama_buku_var" maxlength="255" minlength="2" autocomplete="off">
                                                    <label class="form-label">Nama Buku</label>
                                                </div>
                                                <div class="help-info">Min. 2, Max. 255 characters</div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Kategori Buku</label>
                                                <select class="form-control show-tick" name="id_jenis_buku_int" id="id_jenis_buku_int">
                                                    <?php 
                                                    $sql = "SELECT * FROM t_mtr_jenis_buku";
                                                    $result = mysqli_query($conn, $sql);
                                                
                                                    while($row = mysqli_fetch_assoc($result)){
                                                        ?>
                                                        <option value="<?= $row['id_jenis_buku_seq'] ?>">
                                                            <?= $row['nama_jenis_buku_var'] ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="number" class="form-control" name="harga_int" id="harga_int" min="0" autocomplete="off">
                                                    <label class="form-label">Harga Buku</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <span class="">
                                                    <input type="hidden" name="status_rekomendasi_boo" value="0" />
                                                    <input type="checkbox" class="filled-in" name="status_rekomendasi_boo" id="status_rekomendasi_boo" value="1">
                                                    <label for="status_rekomendasi_boo">Direkomendasi</label>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="ubah" class="btn bg-green btn-link waves-effect">SIMPAN PERUBAHAN</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Ubah Detail Data -->
                        <div class="modal fade" id="modalUbahDetail" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">

                                        <input type="hidden" name="id_buku_int" class="id_buku_int" readonly="">

                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalUbahDetailLabel">Ubah Detail Buku</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Nomor ISBN</label>
                                                    <input type="text" class="form-control" name="nomor_isbn_var" id="nomor_isbn_var" maxlength="255" minlength="2" autocomplete="off">
                                                </div>
                                                <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Penulis</label>
                                                    <input type="text" class="form-control" name="penulis_var" id="penulis_var" maxlength="255" minlength="2" autocomplete="off">
                                                </div>
                                                <div class="help-info">Min. 2, Max. 255 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Penerbit</label>
                                                    <input type="text" class="form-control" name="penerbit_var" id="penerbit_var" maxlength="100" minlength="2" autocomplete="off">
                                                </div>
                                                <div class="help-info">Min. 2, Max. 100 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Tahun Terbit</label>
                                                    <input type="text" class="form-control" name="tanggal_terbit_var" id="tanggal_terbit_var" maxlength="4" minlength="4" autocomplete="off">
                                                </div>
                                                <div class="help-info">Contoh : 2018</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Deskripsi</label>
                                                    <textarea rows="4" class="form-control no-resize" name="deskripsi_buku_txt" id="deskripsi_buku_txt"></textarea>
                                                </div>
                                                <div class="help-info">Min. 2, Max. 255 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Jumlah Halaman</label>
                                                    <input type="number" class="form-control" name="jumlah_halaman_int" id="jumlah_halaman_int" autocomplete="off">
                                                </div>
                                                <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Jenis Bahan Cover</label>
                                                    <input type="text" class="form-control" name="jenis_bahan_cover_buku_var" id="jenis_bahan_cover_buku_var" maxlength="30" minlength="2" autocomplete="off">
                                                </div>
                                                <div class="help-info">Min. 2, Max. 30 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Dimensi Buku</label>
                                                    <input type="text" class="form-control" name="dimensi_buku_var" id="dimensi_buku_var" maxlength="30" minlength="2" autocomplete="off">
                                                </div>
                                                <div class="help-info">Min. 2, Max. 30 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Bahasa</label>
                                                    <input type="text" class="form-control" name="bahasa_var" id="bahasa_var" maxlength="50" minlength="2" autocomplete="off">
                                                </div>
                                                <div class="help-info">Min. 2, Max. 50 characters</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Stok</label>
                                                    <input type="number" class="form-control" name="stok_int" id="stok_int" min="0" autocomplete="off" require>
                                                </div>
                                                <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Tersedia Pada Tanggal</label>
                                                    <input type="text" class="datepicker form-control" name="tersedia_pada_tanggal_dtm" id="tersedia_pada_tanggal_dtm" autocomplete="off" placeholder="Pilih Tanggal">
                                                </div>
                                                <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Diskon (%)</label>
                                                    <input type="number" class="form-control" name="diskon_int" id="diskon_int" min="0" autocomplete="off" require>
                                                </div>
                                                <div class="help-info">Nominal Diskon dalam persen (%)</div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Tanggal Mulai Diskon</label>
                                                    <input type="text" class="datepicker form-control" name="tanggal_mulai_diskon_dtm" id="tanggal_mulai_diskon_dtm" autocomplete="off" placeholder="Pilih Tanggal">
                                                </div>
                                                <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <label class="">Tanggal Akhir Diskon</label>
                                                    <input type="text" class="datepicker form-control" name="tanggal_akhir_diskon_dtm" id="tanggal_akhir_diskon_dtm" autocomplete="off" placeholder="Pilih Tanggal">
                                                </div>
                                                <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="ubah_detail" class="btn bg-green btn-link waves-effect">SIMPAN PERUBAHAN</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Detail Data View -->
                        <div class="modal fade" id="modalDetailView" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalDetailViewLabel">Detail Buku</h4>
                                        </div>
                                        <div class="modal-body">
                                            <span id="detail_buku"></span>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Hapus Data -->
                        <div class="modal fade" id="modalHapus" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalHapusLabel">Hapus Data</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Apakah Anda yakin akan menghapus Data ini?</h5>
                                            <input type="hidden" name="id_buku_seq" class="id_buku_seq" readonly="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="hapus" class="btn bg-red btn-link waves-effect">HAPUS DATA</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Ubah Cover Buku Data -->
                        <div class="modal fade" id="modalUbahCover" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalUbahCoverLabel">Perbaharui Cover Buku</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Apakah Anda yakin akan memperbaharui cover buku dengan yang baru ?</h5>
                                            <input type="file" name="gambar_cover_buku_var" id="gambar_cover_buku_var">
                                            <input type="hidden" name="id_buku_int" class="id_buku_int" readonly="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="ubah_cover" class="btn bg-green btn-link waves-effect">SIMPAN PERUBAHAN</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<script>
    function ubah(id){
        $.ajax({
            url: '../components/api/api_buku.php',
            type: 'post',
            data: { "id_buku_seq": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                $('.id_buku_seq').val(data.id_buku_seq);
                $('#nama_buku_var').val(data.nama_buku_var);
                $('#harga_int').val(data.harga_int);
                // $('#id_jenis_buku_int').val(data.id_jenis_buku_int);
                $('#id_jenis_buku_int option[value='+data.id_jenis_buku_int+']').attr('selected', 'selected');
                if(data.status_rekomendasi_boo == 1){
                    $('#status_rekomendasi_boo').prop('checked', true);
                }else{
                    $('#status_rekomendasi_boo').prop('checked', false);
                }        
                $('#modalUbah').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }

    function ubah_detail(id){
        $.ajax({
            url: '../components/api/api_detail_buku.php',
            type: 'post',
            data: { "id_buku_int": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                $('.id_buku_int').val(data.id_buku_int);
                $('#nomor_isbn_var').val(data.nomor_isbn_var);
                $('#penulis_var').val(data.penulis_var);
                $('#penerbit_var').val(data.penerbit_var);
                $('#tanggal_terbit_var').val(data.tanggal_terbit_var);
                $('#deskripsi_buku_txt').val(data.deskripsi_buku_txt);
                $('#jumlah_halaman_int').val(data.jumlah_halaman_int);
                $('#jenis_bahan_cover_buku_var').val(data.jenis_bahan_cover_buku_var);
                $('#dimensi_buku_var').val(data.dimensi_buku_var);
                $('#bahasa_var').val(data.bahasa_var);
                $('#stok_int').val(data.stok_int);
                $('#tersedia_pada_tanggal_dtm').val(data.tersedia_pada_tanggal_dtm);
                $('#diskon_int').val(data.diskon_int);
                $('#tanggal_mulai_diskon_dtm').val(data.tanggal_mulai_diskon_dtm);
                $('#tanggal_akhir_diskon_dtm').val(data.tanggal_akhir_diskon_dtm);
                $('#modalUbahDetail').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }

    function ubah_detail_view(id){
        $.ajax({
            url: '../components/api/api_detail_buku.php',
            type: 'post',
            data: { "id_buku_int": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                
                var detail = 
                    '<table class="table table-stripped">'+
                        '<tr>'+
                            '<td class="text-bold">Nomor ISBN</td>'+
                            '<td>: '+data.nomor_isbn_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Penulis</td>'+
                            '<td>: '+data.penulis_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Penerbit</td>'+
                            '<td>: '+data.penerbit_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Tahun Terbit</td>'+
                            '<td>: '+data.tanggal_terbit_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Deskripsi</td>'+
                            '<td>: '+data.deskripsi_buku_txt+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Jumlah Halaman</td>'+
                            '<td>: '+data.jumlah_halaman_int+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Cover</td>'+
                            '<td>: '+data.jenis_bahan_cover_buku_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Dimensi</td>'+
                            '<td>: '+data.dimensi_buku_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Bahasa</td>'+
                            '<td>: '+data.bahasa_var+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Stok</td>'+
                            '<td>: '+data.stok_int+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Tersedia tanggal</td>'+
                            '<td>: '+data.tersedia_pada_tanggal_dtm+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Diskon</td>'+
                            '<td>: '+data.diskon_int+' %</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Tanggal Mulai Diskon</td>'+
                            '<td>: '+data.tanggal_mulai_diskon_dtm+'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td class="text-bold">Tanggal Akhir Diskon</td>'+
                            '<td>: '+data.tanggal_akhir_diskon_dtm+'</td>'+
                        '</tr>'+
                    '</table>';
                $('#detail_buku').html(detail);
                $('#modalDetailView').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }

    function hapus(id){
        $('.id_buku_seq').val(id);
        $('#modalHapus').modal('show');
    }

    function ubah_cover(id){
        $('.id_buku_int').val(id);
        $('#modalUbahCover').modal('show');
    }
</script>