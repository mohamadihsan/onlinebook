<?php

// Init Nama Menu 
$menu = 'data penjualan';

// Load Modul 
include_once '../components/global/module.php';
include_once '../components/global/function.php';
include_once '../components/template/header.php';
include_once '../components/template/sidebar.php';
include_once '../components/template/footer.php';
include_once 'global/connection.php';

$conn = connect_to_server();

if (isset($_POST['konfirmasi'])) {
    $id_pembelian_int = $_POST['id_pembelian_int'];
    $nama_bank_var = mysqli_real_escape_string($conn, $_POST['nama_bank_var']);
    $nama_rekening_pembeli_var = mysqli_real_escape_string($conn, $_POST['nama_rekening_pembeli_var']);
    $tanggal_pembayaran_dtm = mysqli_real_escape_string($conn, $_POST['tanggal_pembayaran_dtm']);
    $tanggal_validasi_pembayaran_dtm = date('Y-m-d H:i:s');
    // $di_validasi_oleh_int = $_SESSION['id_user_int'];
    $di_validasi_oleh_int = 1;

    $sql = "INSERT INTO t_trx_validasi_pembayaran 
            (
                id_pembelian_int, 
                total_pembelian_int, 
                nama_bank_var, 
                nama_rekening_pembeli_var, 
                tanggal_pembayaran_dtm, 
                di_validasi_oleh_int, 
                status_pembayaran_boo
            )
    VALUES (
            ".$id_pembelian_int.", 
            ".$total_pembelian_int.",
            '".$nama_bank_var."',
            '".$nama_rekening_pembeli_var."',
            '".$tanggal_pembayaran_dtm."',
            ".$di_validasi_oleh_int.",
            ".$status_pembayaran_boo.",
            )";

    if (mysqli_query($conn, $sql)) {

        // update
        $sql = "UPDATE t_trx_pembelian
        SET status_pembayaran_boo = '".$status_pembayaran_boo."', 
            id_user_int = ".$di_validasi_oleh_int.",
            tanggal_validasi_pembayaran_dtm = '".$tanggal_validasi_pembayaran_dtm."'
        WHERE id_pembelian_seq = $id_pembelian_int";

        mysqli_query($conn, $sql);
        ?><script>msg_berhasil('Data pembelian telah divalidasi! Status Pembayaran telah diterima...')</script><?php
    } else {
        ?><script>msg_gagal('Gagal memvalidasi status pembayaran...')</script><?php
    }
}

?>
<title>Data Penjualan</title>

<section class="content">
    <div class="container-fluid">
        <!-- <div class="block-header">
            <h2>
                BUKU ONLINE
                <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
            </h2>
        </div> -->
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Data Penjualan
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <!-- <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul> -->
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <form action="" method="post">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="">Filter</label>
                                            <input type="text" class="datepicker form-control" name="start_date" id="start_date" autocomplete="off" placeholder="Pilih Tanggal" required>
                                        </div>
                                        <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                    </div>    
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="">.</label>
                                            <input type="text" class="datepicker form-control" name="end_date" id="end_date" autocomplete="off" placeholder="Pilih Tanggal" required>
                                        </div>
                                        <!-- <div class="help-info">Min. 2, Max. 255 characters</div> -->
                                    </div>    
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <button type="submit" name="filter" class="btn btn-sm">Filter</button>
                                </div>
                            </form><hr>
                            <br/><br/><br/><br/>
                            
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <?php  
                                if (isset($_POST['filter'])) {
                                        
                                    $start_date = mysqli_real_escape_string($conn, date('Y-m-d', strtotime($_POST['start_date'])));
                                    $end_date = mysqli_real_escape_string($conn, date('Y-m-d', strtotime($_POST['end_date'])));

                                    $where = "AND t_trx_pembelian.created_on_dtm BETWEEN '".$start_date."' AND '".$end_date."'";

                                    $title = '<h4 class="text-center">Data Penjualan Tanggal '.tanggal_indo($start_date).' - '.tanggal_indo($end_date).' </h4><hr>';
                                }else{
                                    $where = '';
                                    $title = '';
                                }

                                echo $title;
                                ?>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Invoice</th>
                                        <th class="text-center">Customer</th>
                                        <th class="text-center">Batas Pembayaran</th>
                                        <th class="text-center">Total Pembelian</th>
                                        <th class="text-center">Tgl Pembayaran</th>
                                        <th class="text-center">Tgl Transaksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT 
                                                t_trx_pembelian.id_pembelian_seq,
                                                t_trx_pembelian.invoice_pembelian_var,
                                                t_trx_pembelian.total_pembelian_int,
                                                t_trx_pembelian.status_pembayaran_boo,
                                                t_trx_pembelian.batas_akhir_pembayaran_dtm,
                                                t_trx_pembelian.tanggal_validasi_pembayaran_dtm,
                                                t_mtr_detail_user.fullname_var,
                                                t_trx_pembelian.created_on_dtm 
                                            FROM t_trx_pembelian
                                            LEFT JOIN t_mtr_user ON t_mtr_user.id_user_seq = t_trx_pembelian.id_user_int
                                            LEFT JOIN t_mtr_detail_user ON t_mtr_detail_user.id_user_int = t_mtr_user.id_user_seq
                                            WHERE t_trx_pembelian.status_pembayaran_boo = 1 $where";
                                    $result = mysqli_query($conn, $sql);
                                
                                    if (mysqli_num_rows($result) > 0) {
                                        $no = 1;
                                        while($row = mysqli_fetch_assoc($result)){
                                            if ($row['status_pembayaran_boo'] == 0) {
                                                $status_pembayaran_boo = '<span class="label bg-red">Belum Dibayar</span>';
                                            }else{
                                                $status_pembayaran_boo = '<span class="label bg-green">Sudah Dibayar</span>';
                                            }
                                            $batas_akhir_pembayaran_dtm = tanggal_indo(date('Y-m-d', strtotime($row['batas_akhir_pembayaran_dtm'])));

                                            if ($row['tanggal_validasi_pembayaran_dtm'] != null) {
                                                $tanggal_validasi_pembayaran_dtm = tanggal_indo(date('Y-m-d', strtotime($row['tanggal_validasi_pembayaran_dtm'])));    
                                            }else{
                                                $tanggal_validasi_pembayaran_dtm = '';
                                            }
                                            if ($row['created_on_dtm'] != null) {
                                                $created_on_dtm = tanggal_indo(date('Y-m-d', strtotime($row['created_on_dtm'])));    
                                            }else{
                                                $created_on_dtm = '';
                                            }
                                            
                                            ?>
                                            <tr>
                                                <td class="text-center" style="white-space: nowrap">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="detail_penjualan_buku(<?= $row['id_pembelian_seq'] ?>)" title="Detail">
                                                            <i class="material-icons">chrome_reader_mode</i> 
                                                        </button>
                                                    </div>
                                                </td>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td><?= $row['invoice_pembelian_var'] ?></td>
                                                <td><?= $row['fullname_var'] ?></td>
                                                <td class="text-center"><?= $batas_akhir_pembayaran_dtm ?></td>
                                                <td class="text-right"><?= rupiah($row['total_pembelian_int']) ?></td>
                                                <td class="text-center"><?= $tanggal_validasi_pembayaran_dtm ?></td>
                                                <td class="text-center"><?= $created_on_dtm ?></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="7" class="text-center">Data Tidak Ditemukan</td>
                                        </tr>
                                        <?php
                                    }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                                    
                        <!-- Modal Validasi Data -->
                        <div class="modal fade" id="modalValidasi" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="" id="form_advanced_validation" method="post">

                                        <input type="hidden" name="id_pembelian_seq" class="id_pembelian_seq" readonly="">

                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalValidasiLabel">Validasi Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group form-float">
                                                <span id="total_pembelian_int"></span>
                                                <div class="form-line">
                                                    <label class="">Tanggal Pembayaran :</label>
                                                    <div class="form-line">
                                                        <input type="text" class="datetimepicker form-control" name="tanggal_pembayaran_dtm" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="ubah" class="btn bg-green btn-link waves-effect">SIMPAN</button>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                                    
                        <!-- Modal Detail Pembayaran Data -->
                        <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="modalDetailLabel">Detail Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            <span id="detail_pembayaran"></span>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>

<script>

    function detail_penjualan_buku(id){
        $.ajax({
            url: '../components/api/api_detail_penjualan_buku.php',
            type: 'post',
            data: { "id_pembelian_seq": id },
            success: function( response, textStatus, jQxhr ){
                var json = response,
                data = JSON.parse(json);
                
                // var detail = 
                //     '<table class="table table-stripped">'+
                //         '<tr>'+
                //             '<td class="text-bold">Nama Buku</td>'+
                //             '<td>: '+data.nama_buku_var+'</td>'+
                //         '</tr>'+
                //         '<tr>'+
                //             '<td class="text-bold">Jml. Pembelian</td>'+
                //             '<td>: '+data.jumlah_beli_int+'</td>'+
                //         '</tr>'+
                //         '<tr>'+
                //             '<td class="text-bold">Diskon</td>'+
                //             '<td>: '+data.diskon_int+' %</td>'+
                //         '</tr>'+
                //         '<tr>'+
                //             '<td class="text-bold">Harga Sebelum Diskon</td>'+
                //             '<td>: Rp.'+data.harga_buku_sebelum_diskon_int+'</td>'+
                //         '</tr>'+
                //         '<tr>'+
                //             '<td class="text-bold">Harga Sesudah Diskon</td>'+
                //             '<td>: Rp.'+data.harga_buku_sesudah_diskon_int+'</td>'+
                //         '</tr>'+
                //         '<tr>'+
                //             '<td class="text-bold">Sub Total</td>'+
                //             '<td>: '+data.sub_total_int+'</td>'+
                //         '</tr>'+
                //     '</table>';

                var detail = 
                    '<table class="table table-stripped">'+
                        '<tr>'+
                            '<th class="text-bold text-center">Nama Buku</th>'+
                            '<th class="text-bold text-center">Jml. Pembelian</th>'+
                            '<th class="text-bold text-center">Diskon</th>'+
                            '<th class="text-bold text-center">Harga Sebelum Diskon</th>'+
                            '<th class="text-bold text-center">Harga Sesudah Diskon</th>'+
                            '<th class="text-bold text-center">Sub Total</th>'+
                        '</tr>';
                        for (var i = 0; i < data.length; i++) {
                            detail += '<tr>';
                            detail += '<td> '+data[i].nama_buku_var+'</td>';
                            detail += '<td> '+data[i].jumlah_beli_int+'</td>';
                            detail += '<td> '+data[i].diskon_int+' %</td>';
                            detail += '<td> '+formatRupiah(data[i].harga_buku_sebelum_diskon_int, 'Rp.')+'</td>';
                            detail += '<td> '+formatRupiah(data[i].harga_buku_sesudah_diskon_int, 'Rp.')+'</td>';
                            detail += '<td> '+formatRupiah(data[i].sub_total_int, 'Rp.')+'</td>';
                            detail += '</tr>';
                        }
                        
                    detail += '</table>';    
                $('#detail_pembayaran').html(detail);
                $('#modalDetail').modal('show');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                msg_info('Terjadi kesalahan saat Request data ke server...')
            }
        });
    }
</script>