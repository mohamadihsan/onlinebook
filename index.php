<?php
	// ================== HALAMAN FRONTEND ====================

	// Load Modul 
	require_once('components/global/connection.php');
	require_once('components/global/authentication.php');



	// Cek Koneksi ke Server
	$connection_status = connect_to_server();
	if ($connection_status == false) {
		
		// munculkan halaman gagal terhubung ke server
		include 'components/error/connection_failed.php';

	}else{

		// munculkan halaman awal frontend
		include 'pages/index.html';

	}
?>